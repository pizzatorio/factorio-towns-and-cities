data:extend({
    {
        type = "item",
        name = "city-logistic-chest-requester",
        icons = {
            {
                icon = "__base__/graphics/icons/logistic-chest-requester.png",
                tint = { r = 1, g = 0, b = 0, a = 0.3 }
            },
        },
        icon_size = 32,
        subgroup = "logistic-network",
        order = "b[storage]-e[city-logistic-chest-requester]",
        place_result = "city-logistic-chest-requester",
        stack_size = 50,
        logistic_slots_count = 1
    },
})

