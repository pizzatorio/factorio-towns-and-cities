data:extend({
    {
        type = "logistic-container",
        name = "city-logistic-chest-requester",
        icons = {
            {
                icon = "__base__/graphics/icons/logistic-chest-requester.png",
                tint = { r = 1, g = 0, b = 0, a = 0.3 }
            },
        },
        icon_size = 32,
        flags = { "placeable-player", "player-creation" },
        minable = {
            hardness = 0.2,
            mining_time = 0.5,
            result = "logistic-chest-requester"
        },
        max_health = 350,
        corpse = "small-remnants",
        collision_box = { { -0.35, -0.35 }, { 0.35, 0.35 } },
        selection_box = { { -0.5, -0.5 }, { 0.5, 0.5 } },
        resistances =
        {
            {
                type = "fire",
                percent = 90
            },
            {
                type = "impact",
                percent = 60
            }
        },
        fast_replaceable_group = "container",
        inventory_size = 1,
        logistic_mode = "requester",
        logistic_slots_count = 1,
        open_sound = { filename = "__base__/sound/metallic-chest-open.ogg", volume = 0.65 },
        close_sound = { filename = "__base__/sound/metallic-chest-close.ogg", volume = 0.7 },
        vehicle_impact_sound = { filename = "__base__/sound/car-metal-impact.ogg", volume = 0.65 },
        picture =
        {
            filename = "__base__/graphics/entity/logistic-chest/logistic-chest-requester.png",
            priority = "extra-high",
            width = 38,
            height = 32,
            shift = { 0.09375, 0 }
        },
        circuit_wire_connection_point = circuit_connector_definitions["chest"].points,
        circuit_connector_sprites = circuit_connector_definitions["chest"].sprites,
        circuit_wire_max_distance = default_circuit_wire_max_distance
    },
})

